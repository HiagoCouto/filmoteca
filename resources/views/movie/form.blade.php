<div class="form-row">
  <div class="form-group col-md-6">
    <label for="name" class="obrigatorio control-label h6">Titulo do Filme</label>
    <input autofocus class="form-control" type="text" placeholder="Nome" name="title" id="title"
      @if(isset($movie))
        value="{{ $movie->title }}"/>
      @else
        value="{{ old('title')}}"/>
      @endif
    {!! $errors->first('title', '<small class="text-danger">:message</small>') !!}
  </div>

  <div class="form-group col-md-6">
    <label for="genres" class="obrigatorio control-label h6">Genêros</label>
    <select class="multiple-selector" name="genres[]" multiple="multiple">
      @foreach($genres as $genre)
        <option value="{{ $genre->id }}">{{ $genre->title }}</option>
      @endforeach
    </select>
    {!! $errors->first('value', '<small class="text-danger">:message</small>') !!}
  </div>
  <div class="col-md-12 col-lg-8">
    <label for="synopsis" class="obrigatorio control-label h6">Resumo</label>
    <div class="form-group" width="100%">
      <textarea name="synopsis" width="100%" id="synopsis" value="www">
      </textarea>
    </div>
  </div>
</div>

<div id="app">
@if(isset($movie))
  <cast-list :roles="{{ $roles }}" :cast="{{ $movie->participants->toJson() }}"></cast-list>
@else
  <cast-list :roles="{{ $roles }}"></cast-list>
@endif
</div>

<div class="form-group">
  <button class="btn btn-primary" type="submit">
    <i class="fas fa-save"></i> {{ $formMode === 'edit' ? 'Salvar alterações' : 'Concluir cadastro' }}
  </button>
</div>

<script src="https://cdn.tiny.cloud/1/kkz10avk1rv0256g2mqjuwu53wah5cxe38v7x98effpfhkzh/tinymce/5/tinymce.min.js"
  referrerpolicy="origin"
>
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@if(isset($movie))
  <script>
    tinymce.init({
      selector: 'textarea#synopsis',
      init_instance_callback: "insert_contents",
      menubar: false
    });

    function insert_contents(inst){
      inst.setContent("{!! $movie->synopsis !!}");
    }

		$(document).ready(function() {
			$('.multiple-selector').select2();
			$('.multiple-selector').val({{ $movie->getGenresIdsAttribute() }}).change();
		});

  </script>
@else
  <script>
    tinymce.init({
      selector: 'textarea#synopsis',
      menubar: false
    });

		$(document).ready(function() {
			$('.multiple-selector').select2();
		});
  </script>
@endif
