@extends('layouts.base')

@section('content')
<div class="card mt-3 mb-3">
  <div class="card">
    <div class="card-header h5">{{ $movie->title }}</div>
    <div class="card-body">
        <a href="{{ route('movie.index') }}" title="Voltar">
          <button class="btn btn-warning btn-sm">
            <i class="fa fa-arrow-left" aria-hidden="true"></i>Voltar
          </button>
        </a>
        <br />
        <br />

        @if(session('success'))
        <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          {{session('success')}}
        </div>
        @endif

        @if ($errors->any())
        <ul class="alert alert-danger">
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
        @endif

        <form
          method="POST"
          action="{{ route('movie.update', $movie->id) }}"
          accept-charset="UTF-8"
          class="form-horizontal"
          enctype="multipart/form-data"
          >
          @method('PUT')
          {{ csrf_field() }}

          @include ('movie.form', ['formMode' => 'edit'])
        </form>
    </div>
  </div>
</div>
@endsection
