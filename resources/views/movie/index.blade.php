@extends('layouts.base')

@section('content')
<div class="card mt-3 mb-3">
  <div class="card">
    <div class="card-header h5">Lista de Filmes</div>
    <div class="card-body">
      <div class="mb-3 d-flex flex-row-reverse">
        <a href="{{ route('movie.create') }}" class="btn btn-success btn-sm" title="Cadastrar Area">
          <i class="fa fa-plus" aria-hidden="true"></i> Cadastrar
        </a>
      </div>
      <div class="mb-3 d-flex flex-row-reverse">
        <a href="{{ route('apiUpdate') }}" class="btn btn-success btn-sm" title="Cadastrar Area">
          <i class="fa fa-plus" aria-hidden="true"></i> Dados da API
        </a>
      </div>
      <div class="row">
        <div class="col-md-12">

            @if(session('success'))
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
                {{session('success')}}
            </div>
            @endif

            @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger alert-dismissible flat no-margin">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Whoops!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
      </div>

      <div class="table-responsive">
        <table class="table table-striped table-index table-index-td">
          <thead>
            <tr>
              <th>#</th>
              <th>Titulo</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>
            @foreach($movies as $movie)
            <tr>
              <td class="h6">{{ $movie->id }}</td>
              <td class="h6">{{ $movie->title }}</td>
              <td>
                <a href="{{ route('movie.edit', $movie->id) }}" title="Editar">
                  <button class="btn btn-primary btn-sm mr-1"><i class="fa fa-edit" aria-hidden="true"></i></button>
                </a>
                <form
									method="POST"
									action="{{ route('movie.destroy', $movie->id) }}"
									accept-charset="UTF-8"
									style="display:inline"
									>
									{{ method_field('DELETE') }}
                  {{ csrf_field() }}
									<button
										type="submit"
										class="btn btn-danger btn-sm"
										title="Excluir Disciplina"
										onclick="return confirm('Confirmar exclusão?')"
										>
										<i class="fa fa-trash" aria-hidden="true"></i>
									</button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
