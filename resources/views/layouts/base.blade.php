<!doctype html>
<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('/css/styles.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link rel="shortcut icon" type="image/png" href="{{ url('favicon.png')}}"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('/css/principal.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <title>Filmoteca</title>
</head>

<style>
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__arrow {
        height: 38px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 38px;
    }

    .select2-container--default .select2-selection--single {
        border: 1px solid #ced4da;
    }

    .select2-container {
        width: 100% !important;
    }

    .pagination-container{
        margin-top: 15px;
        display: flex;
        justify-content: center;
    }

    .pagination-container > .pagination{
        margin-bottom: 0;
    }
    .select2-search--inline {
      display: contents;
    }
    .select2-search__field:placeholder-shown {
      width: 100% !important;
    }

</style>

@yield('custom_css')
<script>

    function tabDefault() {
        $('#pesqTab a:first').tab('show');
    }
    (function() {
        $("select2-IDProjetoRelacionado -container").select2({ width: 'resolve' });
    });

    window.addEventListener("load", tabDefault);
    window.addEventListener("load", function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

</script>

<body>
	<div class="wrapper">
		<div id="content">
			<div class="container-fluid">
					@yield('content')
			</div>
		</div>
	</div>
	<script src="{{ asset('js/app.js') }}" defer></script>
	<script src="{{url('/')}}/js/functions.js"></script>
</body>

</html>
