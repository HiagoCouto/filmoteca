<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Movie;
use App\Participant;

class ToAPI extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $contents = Storage::disk('local')->get('selecao.xml');
	    $xml = simplexml_load_string($contents);
	    $data = [];

	    foreach($xml as $item) {
        	$movie = Movie::create([
				'title' => strval($item->titulo),
            	'synopsis' => strval($item->resumo),
			]);

        	$genres = [];
			foreach((array)$item->genero as $index => $title) {
        		$genres[] = $index + 1;
			}
        	$movie->genres()->attach($genres);

			foreach ((array)$item->elenco->ator as $participant) {
				$participant = Participant::create([
					'name' => $participant,
					'role_id' => 2,
					'movie_id' => $movie->id
				]);
        	}
			
	    }
    }
}
