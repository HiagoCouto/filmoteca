<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Roteirista', 'Ator', 'Diretor', 'Produtor'];
        array_map(function ($role) {
          DB::table('roles')->insert([
              'name' => $role,
          ]);
        }, $roles);

        $genres = ['Aventura', 'Documentário', 'Comédia', 'Ação', 'Fantasia'];
        array_map(function ($genre) {
          DB::table('genres')->insert([
              'title' => $genre,
          ]);
        }, $genres);

	$this->call([
            ToAPI::class,
        ]);

    }
}
