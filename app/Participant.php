<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
  protected $fillable = ['name', 'role_id', 'movie_id'];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}
