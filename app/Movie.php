<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $fillable = ['title', 'synopsis'];

    public function genres()
    {
        return $this->belongsToMany('App\Genre');
    }

    public function participants()
    {
        return $this->hasMany('App\Participant');
    }

    public function getGenresIdsAttribute()
    {
        return $this->genres->pluck('id');
    }
}
