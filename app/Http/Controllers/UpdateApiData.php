<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Participant;
use Illuminate\Support\Facades\Storage;

class UpdateApiData extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
		$contents = Storage::disk('local')->get('selecao.xml');
        $xml = simplexml_load_string($contents);

        foreach($xml as $item) {
			$movie = Movie::find($item->attributes()->id);
			if ($movie) {
				$movie->update([
					'title' => strval($item->titulo),
					'synopsis' => strval($item->resumo),
				]);

            	$genres = [];
            	foreach((array)$item->genero as $index => $title) {
                	$genres[] = $index + 1;
            	}

				foreach ($movie->participants as $data) {
    				$data->delete();
				}

        		$movie->genres()->sync($genres);
					foreach ((array)$item->elenco->ator as $participant) {
						$participant = Participant::updateOrCreate([
							'name' => $participant,
							'role_id' => 2,
							'movie_id' => $movie->id
						]);
					}
        	} else {

					$movie = Movie::create([
						'title' => strval($item->titulo),
						'synopsis' => strval($item->resumo),
					]);

					$genres = [];
					foreach((array)$item->genero as $index => $title) {
						$genres[] = $index + 1;
					}
					$movie->genres()->attach($genres);

					foreach ((array)$item->elenco->ator as $participant) {
						$participant = Participant::updateOrCreate([
							'name' => $participant,
							'role_id' => 2,
							'movie_id' => $movie->id
						]);
					}
			}

        }

		$movies = Movie::all();
        return view('movie.index', compact('movies'));
    }
}
