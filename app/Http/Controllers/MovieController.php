<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Genre;
use App\Role;
use App\Participant;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('movie.index', compact('movies')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genres = Genre::all();
        $roles = Role::all();
        return view('movie.create', compact('genres', 'roles')); 
    }

    /**
     * Store a newly created participant in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeParticipant($movieId, $participant)
    {
      $participant = Participant::create([
          'name' => $participant['name'],
          'role_id' => $participant['role'],
          'movie_id' => $movieId
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|string',
            'synopsis' => 'required|string',
            'genres' => 'array',
            'participants' => 'string'
        ]);
        $movie = Movie::create($data);
        $movie->genres()->attach($data['genres']);
        foreach (json_decode($data['participants'], true) as $participant) {
            $participant = Participant::create([
                'name' => $participant['name'],
                'role_id' => $participant['role'],
                'movie_id' => $movie->id
            ]);
        }

        return redirect()->route('movie.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        $genres = Genre::all();
        $roles = Role::all();
        return view('movie.edit', compact('genres', 'roles', 'movie')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $data = $request->validate([
            'title' => 'required|string',
            'synopsis' => 'required|string',
            'genres' => 'array',
            'participants' => 'string'
        ]);
        $movie->update($data);
        $movie->genres()->sync($data['genres']);

        foreach (json_decode($data['participants'], true) as $participant) {
						if (isset($participant['id'])) {
								$finded = Participant::find($participant['id']);

								$finded ->update([
										'name' => $participant['name'],
										'role_id' => $participant['role']
								]);
						} else {
								Participant::create([
										'name' => $participant['name'],
										'role_id' => $participant['role'],
										'movie_id' => $movie->id
								]);
						}
        }

        return redirect()->route('movie.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
				$movie->delete();
        return redirect()->route('movie.index');
    }
}
